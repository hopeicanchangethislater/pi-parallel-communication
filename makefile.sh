#! /bin/bash

g++ -Wall -g --std=gnu++0x -c crc8.c
g++ -Wall -g --std=gnu++0x -c ppc.cpp
g++ -Wall -g --std=gnu++0x -c main.cpp -DMASTER_TEST=true

mkdir debug

g++ -Wall -g -o ./debug/PPC main.o ppc.o crc8.o -lwiringPi -lpthread

#rm crc8.o
#rm ppc.o
rm main.o
