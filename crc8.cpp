// Algorithm implemented from http://www.sunshine2k.de/articles/coding/crc/understanding_crc.html

#include "crc8.h"

unsigned crc8(unsigned char data)
{
    unsigned char shift_register = 0;
    unsigned short int i;
    for (i = 0; i < 16; i++)
    {
        if ((shift_register & 0x80) != 0)
        {
            shift_register <<= 1;
            shift_register |= data >> 7;
            shift_register = shift_register ^ CRC8_POLYNOMIAL;
        }
        else
        {
            shift_register <<= 1;
            shift_register |= data >> 7;
        }
        data <<= 1;
    }
    return shift_register;
}

unsigned crc8_multi(unsigned char *data, unsigned int length)
{
    unsigned char crc = 0;
    unsigned int i;
    for (i = 0; i < length; i++)
    {
        crc ^= data[i];
        crc = crc8(crc);
    }
    return crc;
}