#include <iostream>
#include "ppc.h"
#include <unistd.h>
#include <stdio.h>

//#define MASTER_TEST true


int main()
{
  char recv;

  std::cout << "PPC Test" << std::endl;
  std::cout << "Creating PPC_Connection Object" << std::endl;

  // master = MASTER_TEST, clock = 50, buffers = 1024, checksum = true
  PPC_Connection socket(50, 1024, 1024, true);

  socket.init();

  if (MASTER_TEST)
  {
    std::cout << "Sending byte" << std::endl;
    socket.addToSendBuffer((unsigned char) 1);
    socket.addToSendBuffer((unsigned char) 2);
    socket.addToSendBuffer((unsigned char) 4);
    socket.addToSendBuffer((unsigned char) 8);
    socket.addToSendBuffer((unsigned char) 16);
    socket.addToSendBuffer((unsigned char) 32);
    socket.addToSendBuffer((unsigned char) 64);
    socket.addToSendBuffer((unsigned char) 128);
  }

  for (int i = 0; i < 10; i++)
  {
    recv = socket.getRecvBuffer();
    if (recv)
    {
      printf("%u\n", recv);
    }
    usleep(1000000);
  }

  if (MASTER_TEST)
  {
    std::cout << "Sending byte" << std::endl;
    socket.addToSendBuffer((unsigned char) 1);
    socket.addToSendBuffer((unsigned char) 2);
    socket.addToSendBuffer((unsigned char) 4);
    socket.addToSendBuffer((unsigned char) 8);
    socket.addToSendBuffer((unsigned char) 16);
    socket.addToSendBuffer((unsigned char) 32);
    socket.addToSendBuffer((unsigned char) 64);
    socket.addToSendBuffer((unsigned char) 128);
  }

  for (;;)
  {
    recv = socket.getRecvBuffer();
    if (recv)
    {
      printf("%u\n", recv);
    }
    usleep(1000000);
  }

  return 0;
}

