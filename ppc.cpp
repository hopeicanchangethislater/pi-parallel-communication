#include <wiringPi.h>
#include "ppc.h"
#include "crc8.h"

#include <stdio.h>
#include <stdarg.h>
#include <stdint.h>
#include <stdlib.h>
#include <pthread.h>
#include <vector>
#include <unistd.h>
#include <sys/stat.h>

#define SLOW_CLK_RATE 10
#define PPC_DEBUG true
#define BOOLEAN_TEST false
#define PPC_LOG_DATA true

/*
TODO:
 - Boolean variable test
 - Ping
*/

PPC_Connection::PPC_Connection(long unsigned int clockSpeed = SLOW_CLK_RATE, unsigned int sendBufferSize = 1024, unsigned int recvBufferSize = 1024, bool checksum = true)
{
  ///
  /// Constructor for the PPC_Connection class
  ///

  debug(stdout, "[INFO]\tStarted PPC_Connection constructor\n");
  if (checksum)
  {
    // Generate CRC8 lookup table
    debug(stdout, "[INFO]\tGenerating CRC8 lookup table\n");
    static unsigned char LUT[256];
    for (int i = 0; i < 256; i++)
    {
      LUT[i] = crc8(i);
      debug(stdout, "[CRC8LUT]\t%u => %u\n", i, LUT[i]);
    }
    CRC8LUT = LUT;
  }

//  static const unsigned short int inputsArray[9] = {26, 19, 13, 6, 5, 11, 9, 10, 22};
  static const unsigned short int inputsArray[9] = {26, 22, 10, 9, 11, 5, 6, 13, 19};
  static const unsigned short int outputsArray[9] = {21, 20, 16, 12, 7, 8, 25, 24, 23};

  // Pointers to the arrays above. I can't figure out how to make it work with just arrays.
  inputs = inputsArray;
  outputs = outputsArray;

  char masterFile[] = "/home/pi/master";
  struct stat buf;

  if (!stat(masterFile, &buf))
  {
    master_ = true;
  }
  else
  {
    master_ = false;
  }

  connected = false;
  sBufSize = sendBufferSize;
  sBufSize = recvBufferSize;
  clkSpd = clockSpeed;
  check = checksum;
  ok = false;
  handshakeReceived = false;
  timeout = false;
  checksumError = false;

  debug(stdout, "[INFO]\tConstructor finished\n");
}

void PPC_Connection::init()
{
  ///
  /// Initiates the connection between two Raspberry Pis
  ///

  debug(stdout, "[INFO]\tInit start\n");

  // Check if program is running as root
  if (getuid() == 0)
  {
    // Running as root

    debug(stdout, "[INFO]\tRunning as root\n");
    printf("PPC is being run as root. Don't blame me if shit goes tits up.\n");

    // Initialise wiringPi using Broadcom GPIO pin numbers
    wiringPiSetupGpio();
    debug(stdout, "[INFO]\tWiringPi initialised\n");

    // Enable pull-down resistors on inputs
    for (unsigned int i = 0; i < 8 + 1; i++)
    {
        pullUpDnControl(*(inputs + i), PUD_DOWN);
    }
    debug(stdout, "[INFO]\tPull-downs enabled\n");
  }
  else
  {
    // Export GPIO interface. Allows running as normal user
    char command[19];

    debug(stdout, "[INFO]\tRunning as normal user\n");

    for (unsigned int i = 0; i < 8 + 1; i++)
    {
      sprintf(command, "gpio export %u in", *(inputs + i));
      debug(stdout, "[SYSTEM]\t%s\n", command);
      system(command);
      sprintf(command, "gpio export %u out", *(outputs + i));
      debug(stdout, "[SYSTEM]\t%s\n", command);
      system(command);
    }
    // Initialise wiringPi using /sys/class/gpio interface5
    wiringPiSetupSys();
    debug(stdout, "[INFO]\tWiringPi initialised\n");
  }

//  if (master_)
//  {
//    debug(stdout, "[INFO]\tWe are master. Adding 255 and 0 to send buffer.\n");
//    addToSendBuffer((uint8_t) 255);
//    addToSendBuffer((uint8_t) 0x0);
//  }

  debug(stdout, "[INFO]\tCreating send/receive loop threads\n");
  pthread_t threads[3];
  pthread_create(&threads[0], nullptr, PPC_Connection::recvClockLoopInit, (void*) this);
  pthread_create(&threads[1], nullptr, PPC_Connection::recvLoopInit, (void*) this);
  pthread_create(&threads[2], nullptr, PPC_Connection::sendLoopInit, (void*) this);
  debug(stdout, "[INFO] Initialisation complete\n");
}

// Idea of using instance loops a la Kier Davis
void* PPC_Connection::sendLoopInit(void* what)
{
  PPC_Connection *parent = (PPC_Connection*) what;
  parent->debug(stdout, "[INFO]\tsendLoop thread started\n");
  parent->sendLoop();
  return nullptr;
}

void* PPC_Connection::recvLoopInit(void* what)
{
  PPC_Connection *parent = (PPC_Connection*) what;
  parent->debug(stdout, "[INFO]\trecvLoop thread started\n");
  parent->recvLoop();
  return nullptr;
}

void* PPC_Connection::recvClockLoopInit(void* what)
{
  PPC_Connection *parent = (PPC_Connection*) what;
  parent->debug(stdout, "[INFO]\trecvClockLoop thread started\n");
  parent->recvClockLoop();
  return nullptr;
}

void PPC_Connection::recvClockLoop()
{
  bool isEdge = true;
  unsigned char data;
  unsigned int loops = 0;
  while (1)
  {
    loops = 0;
    debug(stdout, "[CLOCKLOOP]\tWaiting for rising input clock edge\n");
    while (!isEdge)
    {
      loops++;
      if (digitalRead(inputs[0]) == LOW) {break;}
      if (loops > 100)
      {
        usleep(1000000 / (2.5 * clkSpd));
      }
    }
    loops = 0;
    while (digitalRead(inputs[0]) == LOW)
    {
      loops++;
      if (loops > 100)
      {
        usleep(1000000 / (2.5 * clkSpd));
      }
    }
    debug(stdout, "[CLOCKLOOP]\tDetected rising edge\n");
    isEdge = false;

    debug(stdout, "[CLOCKLOOP]\tSquishing bits into a byte\n");
    data = 0;
    for (unsigned short int i = 0; i < 8; i++)
    {
      data |= digitalRead(*(inputs + i + 1));
      if (i != 7)
      {
        data = data << 1;
      }
    }
//    rawRecvBufferMutex.lock();
    rawRecvBuffer.push(data);
//    rawRecvBufferMutex.unlock();
  }
}

void PPC_Connection::sendLoop()
{
  debug(stdout, "[INFO]\tsendLoop instance loop started\n");
  bool commsError = true;
  unsigned int handshakeStage = 0;
  const unsigned char handshakeValues[] = {0b10101010, 0b01010101};
//  unsigned int start, loopStart, loopDelta;
  unsigned int i;
  unsigned char crc;
  unsigned int length;
  std::vector<unsigned char> dataArray;
  bool multibyte;
  unsigned int placesToPush;
  unsigned int loops, loops2 = 0;
  while (1)
  {
    debug(stdout, "[SENDLOOP]\tFirst run or timeout.\n");
    commsError = true;
    handshakeStage = 0;
    sendOk = false;
    sendChecksumError = false;
    sendReady = false;
    multibyte = false;
//    timeoutMutex.unlock();
//    timeoutMutex.lock();
//    timeout = false;
    while (!timeout)
    {
      loops++;
      timeoutMutex.unlock();
//      loopStart = micros();
      sendBufferClearMutex.lock();


      if (sendReady & !timeout)
      {
        loops = 0;
        debug(stdout, "[SENDLOOP]\tsendReady\n");
        sendData(READY);
        sendReady = false;
      }

      else if (sendOk & !timeout)
      {
        loops = 0;
        debug(stdout, "[SENDLOOP]\tsendOk\n");
        sendData(OK);
        sendOk = false;
      }

      else if (sendChecksumError & !timeout)
      {
        loops = 0;
        debug(stdout, "[SENDLOOP]\tsendChecksumError\n");
        sendData(CHECKSUM_ERROR);
        sendChecksumError = false;
      }

      // Handshaking
      else if ((!connected) & master_ & !timeout)
      {
        loops = 0;
        debug(stdout, "[SENDLOOP]\tHandhake not completed. We are master. Stage %u\n", handshakeStage);
        sendData(handshakeValues[handshakeStage]);
        debug(stdout, "[SENDLOOP]\tWaiting for handshakeReceived\n");
//        start = millis();
        while (1)
        {
          handshakeReceivedMutex.lock();
          //debug(stdout, "[SENDLOOP]\thandshakeReceivedMutex locked\n");
          if (handshakeReceived)
          {
            debug(stdout, "[SENDLOOP]\thandshakeReceived\n");
            handshakeReceived = false;
            handshakeReceivedMutex.unlock();
            break;
          }
          handshakeReceivedMutex.unlock();
//          if ((millis() - start) > TIMEOUT_DELAY)
//          {
//            // Timeout
//            debug(stdout, "[SENDLOOP]\tTimeout during handshake (master).\n");
//            timeoutMutex.lock();
//            timeout = true;
//            timeoutMutex.unlock();
//            sendData(TIMEOUT);
//            break;
//          }
        }

        handshakeReceivedDataMutex.lock();
        if ((handshakeReceivedData == handshakeValues[handshakeStage]) & (!timeout))
        {
          handshakeReceivedDataMutex.unlock();
          debug(stdout, "[SENDLOOP]\tSent correct data back\n");
          if (handshakeStage == 1)
          {
            ppc_log(stdout, "[SENDLOOP]\tHandshake complete\n");
            sendData(OK);
            connected = true;
            handshakeStage = 0;
          }
          else
          {
            handshakeStage++;
          }
        }
        else
        {
          handshakeReceivedDataMutex.unlock();
          debug(stdout, "[SENDLOOP]\tHandshake failed. Trying again\n");
          handshakeStage = 0;
        }
      }

      else if ((!connected) & (!master_) & !timeout)
      {
        loops = 0;
        debug(stdout, "[SENDLOOP]\tHandshake not completed. We are not master\n");
        while (1)
        {
          handshakeReceivedMutex.lock();
          if (handshakeReceived)
          {
            debug(stdout, "[SENDLOOP]\tHandshake received\n");
            handshakeReceived = false;
            handshakeReceivedMutex.unlock();
            break;
          }
          handshakeReceivedMutex.unlock();
          if (connected)
          {
            break;
          }
          // Implement timout
//          if ((millis() - start) > TIMEOUT_DELAY)
//          {
//            // Timeout
//            debug(stdout, "[SENDLOOP]\tTimeout during handshake (slave).\n");
//            timeoutMutex.lock();
//            timeout = true;
//            sendData(TIMEOUT);
//            break;
//          }
        }
        debug(stdout, "[SENDLOOP]\tSending back handshake byte\n");
        handshakeReceivedDataMutex.lock();
        sendData(handshakeReceivedData);
        handshakeReceivedDataMutex.unlock();
      }

      else if (!sendBuffer.empty() & !timeout) // If there is data in the buffer to send
      {
        sendBufferMutex.lock();
        loops = 0;
        debug(stdout, "[SENDLOOP]\tData to send\n");
        debug(stdout, "[SENDLOOP]\t\t\t%u\n", sendBuffer.front());
        while (commsError)
        {
          ok = false;
          commsError = false;

          // Putting the bytes that were stored in dataArray back in to the sendBuffer
          if (multibyte)
          {
            debug(stdout, "[SENDLOOP]\tResending multiple bytes\n");
            placesToPush = sendBuffer.size();

            // Append bytes to resend
            debug(stdout, "[SENDLOOP]\tAppending bytes to sendBuffer\n");
            for (i = 0; i < length; i++)
            {
              sendBuffer.push(dataArray[i]);
            }

            // Rotate existing elemets to the back of the queue
            debug(stdout, "[SENDLOOP]\tRotating bytes to front of sendBuffer\n");
            for (i = 0; i < placesToPush; i++)
            {
              sendBuffer.push(sendBuffer.front());
              sendBuffer.pop();
            }
          }
          debug(stdout, "[SENDLOOP]\tChecking sendBuffer length: ");
          length = (unsigned int) sendBuffer.size();
          debug(stdout, "%p\n", sendBuffer.size());
          if (length > 1)
          {
            if (length > 16)
            {
              length = 16;
            }
            dataArray.resize(length);
            debug(stdout, "[SENDLOOP]\tSending MULTIBYTE control byte\n");
            sendData(MULTIBYTE);
            debug(stdout, "[SENDLOOP]\tSending data size\n");
            sendData((unsigned char) length);
            multibyte = true;
          }
          else
          {
            debug(stdout, "[SENDLOOP]\tSending DATA control byte\n");
            sendData(DATA); // Send DATA control byte to other pi
          }

          // Wait for client to be ready for data
          debug(stdout, "[SENDLOOP]\tWaiting for clientReady\n");
          while (!clientReady)
          {
//            if ((millis() - start) > TIMEOUT_DELAY)
//            {
//              // Timeout
//              debug(stdout, "[SENDLOOP]\tTimeout waiting for clientReady.\n");
//              timeoutMutex.lock();
//              timeout = true;
//              sendData(TIMEOUT);
//              break;
//            }
          }
          debug(stdout, "[SENDLOOP]\tclientReady\n");

          // Send data to client
          if (multibyte)
            for (i = 0; i < length; i++)
            {
              dataArray[i] = sendBuffer.front();
              sendBuffer.pop();
              debug(stdout, "[SENDLOOP]\tSending data bytes\n");
              sendData(dataArray[i]);
            }
          else
          {
            debug(stdout, "[SENDLOOP]\tSending data byte\n");
            sendData(sendBuffer.front());
          }
          if (check)
          {
            crc = 0;
            if (multibyte)
            {
              for (i = 0; i < length; i++)
              {
                crc = CRC8LUT[crc ^ dataArray[i]];
              }

              debug(stdout, "[SENDLOOP]\tSending CRC8 byte\n");
              sendData(crc);
            }
            else
            {
              debug(stdout, "[SENDLOOP]\tSending CRC8 byte\n");
              sendData(CRC8LUT[sendBuffer.front()]);
            }
          }

          // If checksums are enabled, wait for OK from client.
          if (check)
          {
            debug(stdout, "[SENDLOOP]\tWaiting for checksum ok\n");
          }
          loops2 = 0;
          while (check & (!ok))
          {
            loops2++;
            if (checksumError)
            {
              debug(stdout, "[SENDLOOP]\tChecksum error\n");
              checksumError = false;
              commsError = true;
              break;
            }
            if (loops2 > 100)
            {
              sleep(0.9 / (clkSpd * 2));
            }
            if (loops2 > 110)
            {
              checksumError = false;
              commsError = true;
              break;
            }
          }
          sendBufferMutex.unlock();
        }
        commsError = true;
        if (!multibyte)
        {
          debug(stdout, "[SENDLOOP]\tRemoving sent byte%c from buffer\n", length > 1 ? 's' : 0x1A);
          sendBuffer.pop(); // Remove the front element of the queue
        }
        else
        {
          multibyte = false;
        }

        sendBufferMutex.unlock();
      }
      sendBufferMutex.unlock();
      sendBufferClearMutex.unlock();
//      loopDelta = (1000000 / clkSpd) - (micros() - loopStart);
//      if (loopDelta > 0)
//      {
//        delayMicroseconds(loopDelta / 2);
//      }
      if (loops > 100)
      {
        usleep(1000000 / clkSpd);
      }
    }
    debug(stdout, "[SENDLOOP]\treturn\n");
  }
}

void PPC_Connection::recvLoop()
{
  debug(stdout, "[INFO]\trecvLoop instance loop started\n");
  bool timeout = false;
  unsigned char data;
  unsigned char dataToAdd;
  bool nextByteIsData;
  bool nextByteIsChecksum;
  bool multibyte = false;
  unsigned int length;
  unsigned int i;
  unsigned int remaining;
  unsigned char crc;
  unsigned int loops = 0;
  bool checksumOverride = false;
  std::vector<unsigned char> bytesToAdd;
//  unsigned int loopStart, loopDelta;
  while (1)
  {
    debug(stdout, "[RECVLOOP]\tFirst run or timeout.\n");
//    timeoutMutex.lock();
//    timeout = false;
//    timeoutMutex.unlock();
    nextByteIsData = false;
    nextByteIsChecksum = false;
    while (!timeout)
    {
//      loopStart = micros();
      loops = 0;
      while (1)
      {
        loops++;
        if (!rawRecvBuffer.empty())
        {
          data = rawRecvBuffer.front();
          rawRecvBuffer.pop();
          break;
        }
        if (loops > 100)
        {
          usleep(1000000 / clkSpd);
        }
        if ((nextByteIsChecksum | nextByteIsData) & (loops > 110) & check)
        {
          nextByteIsChecksum = false;
          nextByteIsData = false;
          multibyte = false;
          sendChecksumError = true;
        }
      }

      ppc_log(stdout, "[IN]\t%d\n", data);

      // If we're expecting data length
      if (multibyte & (!nextByteIsData) & (!nextByteIsChecksum))
      {
        debug(stdout, "[RECVLOOP]\tExpecting byte received to contain data length\n");
        length = (unsigned int) data;
        remaining = length;
        bytesToAdd.resize(length);
        nextByteIsData = true;
        crc = 0;
        sendReady = true;
      }

      // If we're expecting multiple data bytes
      else if (multibyte & nextByteIsData)
      {
        bytesToAdd.at(length - remaining) = data;
        remaining--;
        if (check)
        {
          crc = CRC8LUT[crc ^ data];
        }
        if (check & (remaining == 0))
        {
          nextByteIsData = false;
          multibyte = true;
          nextByteIsChecksum = true;
        }
        else if (remaining == 0)
        {
          recvBufferMutex.lock();
          for (i = 0; i < length; i++)
          {
            recvBuffer.push(bytesToAdd.at(i));
          }
          recvBufferMutex.unlock();
        }
      }

      // If we're expecting data
      else if (nextByteIsData)
      {
        debug(stdout, "[RECVLOOP]\tExpecting byte received to contain data\n");
        nextByteIsData = false;
        if (check)
        {
          nextByteIsChecksum = true;
          dataToAdd = data;
          crc = CRC8LUT[data];
          debug(stdout, "[RECVLOOP]\tNow expecting checksum byte\n");
        }
        else
        {
          debug(stdout, "[RECVLOOP]\tAdding to recvBuffer\n");
          recvBufferMutex.lock();
          if (recvBuffer.size() <= rBufSize)
          {
            recvBuffer.push(data);
          }
          else
          {
            debug(stdout, "\t[ERROR] recvBuffer size limit reached\n");
          }
          recvBufferMutex.unlock();
        }
      }

      // If we're expecting a checksum byte
      else if (nextByteIsChecksum)
      {
        nextByteIsChecksum = false;
        debug(stdout, "[RECVLOOP]\tExpecting byte received to contain CRC8\n");

        if (crc == data)
        {
          debug(stdout, "[RECVLOOP]\tChecksum match on incoming data. Incoming = %d, Calculated = %d\n", data, crc);
          // debug(stdout, "\tSending OK\n");
          sendOk = true;
          if (!multibyte)
          {
            ppc_log(stdout, "[RECVLOOP]\tAdding single byte to recvBuffer: ");
            recvBufferMutex.lock();
            ppc_log(stdout, "%u\n", dataToAdd);
            recvBuffer.push(dataToAdd);
            recvBufferMutex.unlock();
          }
          else
          {
            ppc_log(stdout, "[RECVLOOP]\tAdding %d bytes to recvBuffer\n", bytesToAdd.size());
            recvBufferMutex.lock();
            for (i = 0; i < length; i++)
            {
              ppc_log(stdout, "[RECVLOOP]\tAdding %d to recvBuffer\n", bytesToAdd.at(i));
              recvBuffer.push(bytesToAdd.at(i));
            }
            recvBufferMutex.unlock();
            multibyte = false;
          }
        }
        else
        {
          debug(stdout, "[RECVLOOP]\tChecksum error on incoming data. Incoming = %d, Calculated = %d\n", data, crc);
          // debug(stdout, "\tSending CHECKSUM_ERROR\n");
          checksumErrorMutex.lock();
          sendChecksumError = true;
          checksumErrorMutex.unlock();
        }
        checksumErrorMutex.unlock();
      }

      // If we receive an OK control byte (data was transmitted ok)
      else if (data == OK)
      {
        debug(stdout, "[RECVLOOP]\tReceived OK control byte.\n");
        if (!connected)
        {
          ppc_log(stdout, "[RECVLOOP]\tHandshake completed.\n");
          connected = true;
        }
        ok = true;
      }

      // If the handshake has not been completed
      else if (!connected)
      {
        debug(stdout, "[RECVLOOP]\tHandshake not completed. handshakeReceived\n");
        handshakeReceivedMutex.lock();
        handshakeReceivedDataMutex.lock();

        handshakeReceived = true;
        handshakeReceivedData = data;

        handshakeReceivedMutex.unlock();
        handshakeReceivedDataMutex.unlock();
      }

      // If we receive a DATA control byte (next byte will be data)
      else if (data == DATA)
      {
        debug(stdout, "[RECVLOOP]\tReceived DATA control byte.\n\t\tNext byte received is expected to contain data\n");
        nextByteIsData = true;
        sendReady = true;
      }

      // If we receive a MULTIBYTE control byte (next byte will be length)
      else if (data == MULTIBYTE)
      {
        debug(stdout, "[RECVLOOP]\tReceived MULTIBYTE control byte.\n\t\tNext byte received is expected to contain length\n");
        multibyte = true;
      }

      // If we receive a READY control byte (we can now send data)
      else if (data == READY)
      {
        debug(stdout, "[RECVLOOP]\tReceived READY control byte.\n\t\tWe can now send a data byte\n");
        clientReady = true;
      }

      // If we receive a CHECKSUM_ERROR control byte (resend data)
      else if (data == CHECKSUM_ERROR)
      {
        debug(stdout, "[RECVLOOP]\tReceived CHECKSUM_ERROR control byte.\n\t\tWe can now resend the data byte\n");
        checksumError = true;
      }

      else if (data == TIMEOUT)
      {
        debug(stdout, "[RECVLOOP]\tClient timed out.\n");
      }

//      loopDelta = (1000000 / clkSpd) - (micros() - loopStart);
//      if (loopDelta > 0)
//      {
//        delayMicroseconds(loopDelta / 2);
//      }
    }
  }
}

void PPC_Connection::sendData(unsigned char data)
{
  ppc_log(stdout, "[OUT]\t%u\n", data);
  for (short unsigned int i = 0; i < 8; i++)
  {
    digitalWrite(*(outputs + i + 1), ((data & intPow(2, i)) >> i));
//    debug(stdout, "[OUT]\tPin %d set to %d\n", *(outputs + i + 1), ((data & intPow(2, i)) >> i));
  }
  clkSpdMutex.lock();
  digitalWrite(outputs[0], HIGH);
  delay(1000 / clkSpd);
  digitalWrite(outputs[0], LOW);
  delay(1000 / clkSpd);
  clkSpdMutex.unlock();
}

int PPC_Connection::setRecvBufferSize(int bufferSize)
{
  debug(stdout, "[INFO]\tsetRecvBufferSize\n");
  rBufSize = bufferSize;
  int oldSize;
  int i = 0;
  std::queue<unsigned char> newQueue;
  recvBufferMutex.lock();
  oldSize = recvBuffer.size();
  if (bufferSize < oldSize)
  {
    for (i = 0; (bufferSize - i) > 0; i++)
    {
      newQueue.push(recvBuffer.front());
      recvBuffer.pop();
    }
    recvBuffer.swap(newQueue);
  }
  recvBufferMutex.unlock();
  
  // Return the number of elements lost
  if ((bufferSize - i) > 0)
  {
    return (bufferSize - i);
  }
  return 0;
}
  
int PPC_Connection::setSendBufferSize(int bufferSize)
{
  debug(stdout, "[INFO]\tsetSendBufferSize\n");
  sBufSize = bufferSize;
  int oldSize;
  int i;
  std::queue<unsigned char> newQueue;
  sendBufferMutex.lock();
  oldSize = sendBuffer.size();
  if (bufferSize < oldSize)
  {
    for (i = 0; (bufferSize - i) > 0; i++)
    {
      newQueue.push(sendBuffer.front());
      sendBuffer.pop();
    }
    sendBuffer.swap(newQueue);
  }
  sendBufferMutex.unlock();
  
  // Return the number of elements lost
  if ((bufferSize - i) > 0)
  {
    return (bufferSize - i);
  }
  return 0;
}
  
void PPC_Connection::setClockSpeed(long unsigned int clockSpeed)
{
  debug(stdout, "[INFO]\tsetClockSpeed\n");
  clkSpdMutex.lock();
  clkSpd = clockSpeed;
  clkSpdMutex.unlock();
}

int PPC_Connection::addToSendBuffer(unsigned char data)
{
  ppc_log(stdout, "[INFO] addToSendBuffer\n", data);
  sendBufferMutex.lock();
  ppc_log(stdout, "[INFO] Adding %d to sendBuffer\n", data);
  if (sendBuffer.size() < sBufSize)
  {
    sendBuffer.push(data);
    sendBufferMutex.unlock();
    debug(stdout, "\tAdded 1 element\n");
    return 1;
  }
  else
  {
    sendBufferMutex.unlock();
    debug(stdout, "\t[ERROR] sendBuffer size limit reached\n");
    return 0; // No need to throw an exception - can be inferred from return value of zero.
  }
}

int PPC_Connection::addToArbiBuffer(char data)
{
  sendBufferMutex.lock();
  debug(stdout, "[INFO] Adding %d to arbitrary buffer\n", data);
  sendBuffer.push(data);
  sendBufferMutex.unlock();
  debug(stdout, "\tAdded 1 element\n");
  return 1;
}

int PPC_Connection::addToSendBuffer(unsigned char data[], unsigned int length)
{
  unsigned int i = 0;
  for (i = 0; i < length; i++)
  {
    debug(stdout, "[INFO] Adding %d to sendBuffer\n", data[i]);
    if (!addToSendBuffer(data[i]))
    {
      debug(stdout, "[ERROR] sendBuffer size limit reached\n");
      break;
    }
  }
  debug(stdout, "[INFO]Added %d element%c to sendBuffer\n", i, i == 1 ? 0x1A : 's');
  return i;
}

unsigned char PPC_Connection::getRecvBuffer()
{
//  ppc_log(stdout, "[INFO]\tgetRecvBuffer\n");
  unsigned char data = '\0';
//  recvBufferMutex.lock();
  if (!recvBuffer.empty())
  {
    ppc_log(stdout, "[INFO]\trecvBuffer populated\n");
    data = recvBuffer.front();
    recvBuffer.pop();
  }
//  recvBufferMutex.unlock();
  return data;
}

bool PPC_Connection::recvBufferPopulated()
{
  return !recvBuffer.empty();
}

void PPC_Connection::clearSendBuffer()
{
  debug(stdout, "[INFO]\tclearSendBuffer\n");
  sendBufferMutex.lock();
  sendBufferClearMutex.lock();
  sendBuffer = std::queue<unsigned char>();
  sendBufferMutex.unlock();
  sendBufferClearMutex.unlock();
}

void PPC_Connection::clearRecvBuffer()
{
  debug(stdout, "[INFO]\tclearRecvBuffer\n");
  recvBufferMutex.lock();
  recvBufferClearMutex.lock();
  recvBuffer = std::queue<unsigned char>();
  recvBufferMutex.unlock();
  recvBufferClearMutex.unlock();
}

long int PPC_Connection::intPow(const long int x, const unsigned long int y)
{
  int a = x;
  if ((a == 0) & (y == 0)) {throw(PPC_Exception("0^0 is undefined."));}
  if (y == 0) {return 1;}
  if (a == 0) {return 0;}
  if (y < 0) {throw(PPC_Exception("Negative exponents not implemented."));}

  for (unsigned short int i = 1; i < y; i++)
  {
    a *= x;
  }
//  debug(stdout, "[INFO]\t%d to the power of %d is equal to %d\n", x, y, a);
  return a;
}

void PPC_Connection::debug(FILE* stream, const char* format, ...)
{
  if (PPC_DEBUG)
  {
    va_list args;
    va_start(args, format);
    debugMutex.lock();
    printf("%ul\t", millis());
    vfprintf(stream, format, args);
    debugMutex.unlock();
    va_end(args);
  }
}

void PPC_Connection::ppc_log(FILE* stream, const char* format, ...)
{
  va_list args;
  va_start(args, format);
  debugMutex.lock();
  printf("%ul\t", millis());
  vfprintf(stream, format, args);
  debugMutex.unlock();
  va_end(args);
}
