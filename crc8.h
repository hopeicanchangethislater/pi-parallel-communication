#ifndef CRC8_H
#define CRC8_H

#define CRC8_POLYNOMIAL 0x9C

unsigned crc8(unsigned char data);
unsigned crc8_multi(unsigned char *data, unsigned int length);

#endif
