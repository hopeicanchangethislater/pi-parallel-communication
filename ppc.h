#ifndef ppc_h
#define ppc_h

#include <queue>
#include <stdexcept>
#include <mutex>

// milliseconds
#define TIMEOUT_DELAY 30000

#define NONE 0
#define DATA 1
#define READY 2
#define CHECKSUM_ERROR 3
#define OK 4
#define MASTER 5
#define SLAVE 6
#define TIMEOUT 7
#define MULTIBYTE 8


/// http://www.goospoos.com/2012/06/exception-class-example-cpp/
class PPC_Exception: public std::exception
{
public:
  PPC_Exception(const char* errMessage): errMessage_(errMessage){}
  const char* what() const throw() {return errMessage_;}

private:
  const char* errMessage_;
};

class PPC_Connection
{
private:

  // Variables
  bool parity;
  bool connected;
  std::queue<unsigned char> sendBuffer;
  std::queue<unsigned char> recvBuffer;
  std::queue<unsigned char> rawRecvBuffer;
  unsigned int sBufSize;
  unsigned int rBufSize;
  unsigned long int clkSpd;
  bool master_;
  bool check;
  int recvCheck;
  bool runRecvLoop;
  bool sendReady;
  bool checksumError;
  bool ok;
  bool sendOk;
  bool sendChecksumError;
  unsigned char handshakeReceivedData;
  bool handshakeReceived;
  bool clientReady;
  unsigned char *CRC8LUT;
  bool timeout;

  // Broadcom GPIO pins used by PPC
  const unsigned short int *inputs;
  const unsigned short int *outputs;
  // Pins 26 and 21 are clocks. Rest are data pins.

  // Mutexes
  std::mutex clkSpdMutex;
  std::mutex sendBufferMutex;
  std::mutex recvBufferMutex;
  std::mutex sendBufferClearMutex;
  std::mutex recvBufferClearMutex;
  std::mutex handshakeReceivedDataMutex;
  std::mutex handshakeReceivedMutex;
  std::mutex debugMutex;
  std::mutex timeoutMutex;
  std::mutex okMutex;
  std::mutex rawRecvBufferMutex;
  std::mutex checksumErrorMutex;

  // Functions
  int getBitDepth();
  bool gettingInput();
  bool testData();
  void sendData(unsigned char data);
  long int intPow(const long int x, const unsigned long int y);
  void debug(FILE* stream, const char* format, ...);
  void ppc_log(FILE* stream, const char* format, ...);
  int addToArbiBuffer(const char data);
  static void* sendLoopInit(void* that);
  static void* recvLoopInit(void* that);
  static void* recvClockLoopInit(void* that);
  void sendLoop();
  void recvLoop();
  void recvClockLoop();


public:
  void init();
  int ping();
  
  // Both return the number of elements lost
  int setRecvBufferSize(int bufferSize);
  int setSendBufferSize(int bufferSize);
  
  void setClockSpeed(unsigned long int clockSpeed);
  
  // Both return the number of elements (bytes) added
  int addToSendBuffer(unsigned char data[], unsigned int length);
  int addToSendBuffer(unsigned char data);
  
  unsigned char getRecvBuffer();
  void clearSendBuffer();
  void clearRecvBuffer();
  bool isConnected();
  bool recvBufferPopulated();

  PPC_Connection(long unsigned int clockSpeed, unsigned int sendBufferSize, unsigned int recvBufferSize, bool checksum);
};

#endif
